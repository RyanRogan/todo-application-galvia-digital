@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3 text-center">Update a task</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('tasks.update', $task->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="first_name"> Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $task->name }}" />
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $task->description }}" />
            </div>

            <div class="form-group">
                <label for="date">Due Date:</label>
                <input type="text" class="form-control" name="date" value="{{ $task->date }}" />
            </div>

            <div class="form-group">
                <label for="priority">Priority:</label>
                <select class="form-control" name="priority">
                  <option value="{{ $task->priority }}"> {{ $task->priority }} (Current Selection) </option>
                  <option value="1">1 (Urgent)</option>
                  <option value="2">2 (Average)</option>
                  <option value="3">3 (Not Urgent)</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success">
                <span class="fa fa-check" aria-hidden="true"></span> Update
            </button>
          <a href="/tasks" class="btn btn-secondary" style="float:right;"><span class="fa fa-arrow-left" aria-hidden="true"></span> Back</a>
        </form>
    </div>
</div>
@endsection
@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3 text-center">Tasks</h1>
    <div>
        <a style="float:left;margin-bottom: 19px;" href="{{ route('tasks.create')}}" class="btn btn-primary"><span class="fa fa-plus" aria-hidden="true"></span> New Task</a>
        <h5 class="font-weight-bold" style="float:right;margin-bottom: 0;"> {{$tasks->count()}} Tasks</h5>
    </div>
  <table class="table table-hover">
    <thead class="font-weight-bold">
        <tr>
          <td>Priority</td>
          <td>Name</td>
          <td>Description</td>
          <td>Due Date</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($tasks as $task)
        <tr>
            <td id="priority-{{$task->priority}}">{{$task->priority}}</td>
            <td>{{$task->name}}</td>
            <td>{{$task->description}}</td>
            <td>{{$task->date}}</td>
            <td>
                <a href="{{ route('tasks.edit',$task->id)}}" class="btn btn-primary"><span class="fa fa-pencil" aria-hidden="true"></span>Edit</a>
            </td>
            <td>
                <form action="{{ route('tasks.destroy', $task->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit"><span class="fa fa-trash" aria-hidden="true"></span>Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection
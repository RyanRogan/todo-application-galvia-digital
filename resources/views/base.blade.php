<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ToDo App</title>
	<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 

	<!-- Font Awesome -->
	<script src="https://kit.fontawesome.com/98fddee540.js" crossorigin="anonymous"></script>
	<style type="text/css">
		.fa{
			margin-right:8px;
		}
		#priority-1{
			background-color: rgb(242,91,99); /* Red */
			width:10px;
			text-align:center;
		}
		#priority-2{
			background-color: rgb(255,162,100); /* Amber (Yellow/Orange) */
			width:10px;
			text-align:center;
		}
		#priority-3{
			background-color: rgb(114,217,108); /* Green */
			width:10px;
			text-align:center;
		}
	</style>
</head>

<body>
  <div class="container">
    @yield('main')
  </div>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>
